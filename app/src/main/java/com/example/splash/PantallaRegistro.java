package com.example.splash;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

public class PantallaRegistro extends AppCompatActivity {

    private EditText numero_telefono;
    private String numero, codigo_pais, numero_movil, verificationId;
    private TextView tvTituloRegistro, tvTituloVerificacion, tvTituloReenviarCodigo;
    private Spinner spinnerPaises;
    private ProgressBar progressBar;
    private FloatingActionButton fab, fab2;
    private ImageView imgRegresar;
    private Spinner paises;
    private String[] nombres_paises = {"+57", "+1", "+52"};
    private int[] banderas_paises = {R.drawable.colombia, R.drawable.us, R.drawable.mex};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);


        numero_telefono = findViewById(R.id.editPhone);
        tvTituloRegistro = findViewById(R.id.tvTituloRegistro);
        tvTituloVerificacion = findViewById(R.id.tvTituloVerificacion);
        spinnerPaises = (Spinner) findViewById(R.id.spinnerPaises);
        tvTituloReenviarCodigo = (TextView) findViewById(R.id.tvTituloReenviarCodigo);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        fab = findViewById(R.id.fab);
        fab2 = findViewById(R.id.fab2);
        imgRegresar = (ImageView) findViewById(R.id.imgRegresar);

        paises = (Spinner) findViewById(R.id.spinnerPaises);



        paises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                codigo_pais = nombres_paises[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                numero = numero_telefono.getText().toString().trim();
                if (numero.isEmpty() || numero.length() < 10) {
                    numero_telefono.setError("Validar número");
                    numero_telefono.requestFocus();
                    return;
                } else {
                    tvTituloRegistro.setVisibility(View.GONE);
                    tvTituloVerificacion.setVisibility(View.VISIBLE);
                    spinnerPaises.setVisibility(View.GONE);
                    numero_telefono.setVisibility(View.GONE);
                    tvTituloReenviarCodigo.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    fab.setVisibility(View.GONE);
                    fab2.setVisibility(View.GONE);
                    imgRegresar.setVisibility(View.VISIBLE);

                    numero_movil = codigo_pais + numero;


                    progressBar.setVisibility(View.VISIBLE);

                }
            }
        });

        numero_telefono.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("RestrictedApi")
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    numero = numero_telefono.getText().toString().trim();
                    if (numero.isEmpty() || numero.length() < 10) {
                        numero_telefono.setError("Validar número");
                        numero_telefono.requestFocus();
                        return true;
                    } else {
                        tvTituloRegistro.setVisibility(View.GONE);
                        tvTituloVerificacion.setVisibility(View.VISIBLE);

                        spinnerPaises.setVisibility(View.GONE);
                        numero_telefono.setVisibility(View.GONE);
                        tvTituloReenviarCodigo.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        fab.setVisibility(View.GONE);
                        fab2.setVisibility(View.GONE);
                        imgRegresar.setVisibility(View.VISIBLE);

                        numero_movil = codigo_pais + numero;



                        progressBar.setVisibility(View.VISIBLE);

                    }
                    return true;
                }
                return false;
            }
        });


        imgRegresar.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                tvTituloRegistro.setVisibility(View.VISIBLE);
                tvTituloVerificacion.setVisibility(View.GONE);

                spinnerPaises.setVisibility(View.VISIBLE);
                numero_telefono.setVisibility(View.VISIBLE);
                tvTituloReenviarCodigo.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                fab.setVisibility(View.VISIBLE);
                fab2.setVisibility(View.GONE);
                imgRegresar.setVisibility(View.GONE);
            }
        });


    }
}
